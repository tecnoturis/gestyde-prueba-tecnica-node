# Prueba técnica gestyde Nodejs

## Guía servicios
Para levantar el servidor en localhost:3000 ejecutar en la consola 
```
npm run dev
```

Entrar en el swagger y ver los servicios disponibles. http://localhost:3000/api-explorer/


Para ejecutar los tests 

```
npm run test
```

## Prueba técnica

Pasos a seguir:

1. Obtener disponibilidad de habitaciones y precios 

2. Obtener un listado con los datos estáticos de hoteles

3. Combinar disponibilidad y datos estáticos de hoteles

4. Mapear los datos a un listado de DTO Hotel

5. Pasar los tests

6. Añadir nuevos tests

7. Opcional:

  * Devolver un código 500 cuando disponibilidad devuelva error

  * Implementar tests que prueben la nueva funcionalidad

## Requisitos:
Pasar lint 
```
npm run lint
```

Pasar compile 
```
npm run compile
```
Pasar tests
```
npm run test
```
## URLS:
Disponibilidad: 

POST http://localhost:3000/api/v1/mock/availability

Listado de datos estáticos de hoteles: 

POST http://localhost:3000/api/v1/mock/hotels

Se valorará:
1. Calidad del código
2. Patrones de diseño utilizados
