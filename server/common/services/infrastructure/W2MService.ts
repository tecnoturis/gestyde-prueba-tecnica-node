import { l } from "../../logger";
import { container } from "../../ioc/container";
import XmlParser from "../utils/XmlParser";
import { Config } from "../../config";
import ResponseData from "../../dto/ResponseData";
import {HttpServiceIface} from "../../ioc/interfaces";
import {TYPES} from "../../ioc/types";

export default class W2MService {
    private readonly endpoint: string;

    public constructor() {
        this.endpoint = Config.getKey('api_url');
    }

    public availabilityByGeo = (providerReq: string): Promise<ResponseData> => {
        //const auth: Auth = this.auth;
        const path = '/availability';
        const url = this.endpoint + path;
        const options = {
            body: providerReq,
        };

        l.trace("W2MService availabilityByGeo input ", (providerReq));
        return this.executeRequest(url, 'post', options, 'availabilityByGeo');
    };


    public getHotels = (providerReq: string): Promise<ResponseData> => {
        //const auth: Auth = this.auth;
        const path = '/hotels';
        const url = this.endpoint + path;
        const options = {
            body: providerReq,
        };

        l.trace("W2MService getHotels input ", (providerReq));
        return this.executeRequest(url, 'post', options, 'getHotels');
    };

    private executeRequest(url, method, opts, operationName): Promise<ResponseData> {
        const http = container.get<HttpServiceIface>(TYPES.HttpService);
        let res = new ResponseData();

        const defaults = {
            headers: {
                'Content-Type': 'text/xml'
            },
            gzip: true,
        };

        const options = {...defaults, ...opts};

        return new Promise((resolve, reject) => {
            http[method](url, options)
                .then(XmlParser.parse)
                .then((response) => {
                    l.trace(`W2MService ${operationName} output`, (response));
                    res.status = 200;
                    res.data = response;
                    resolve(res);
                }).catch(e => {
                    l.error(`W2MService ${operationName} output`, (e));
                    res.status = 503;
                    res.data = undefined;
                    reject(res);
                });
        });

    }
}
