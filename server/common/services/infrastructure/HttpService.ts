/* tslint:disable:@typescript-eslint/no-explicit-any */
import rp from 'request-promise-native';
import {injectable} from "inversify";
import {HttpServiceIface} from "../../ioc/interfaces";

@injectable()
export default class HttpService implements HttpServiceIface{

    public parseOptions = (uri, method, options): void =>{
        options = options || {};
        options.method = method;
        options.url = uri;
        return options;
    };

    public request = (options): Promise<any> => {
        return rp(options);
    };

    public get = (url, options?): Promise<any> =>{
        return this.request(this.parseOptions(url, 'GET', options));
    };
    public post = (url, options?): Promise<any> =>{
        return this.request(this.parseOptions(url, 'POST', options));
    };
    public put = (url, options?): Promise<any> =>{
        return this.request(this.parseOptions(url, 'PUT', options));
    };
    public delete = (url, options?): Promise<any> =>{
        return this.request(this.parseOptions(url, 'DELETE', options));
    };
    public patch = (url, options?): Promise<any> =>{
        return this.request(this.parseOptions(url, 'PATCH', options));
    };
}
