const parseString = require('xml2js').parseString;

export default class XmlParser{
    public static parse (xml: string): Promise<any>{
        const options = {explicitArray: false, mergeAttrs: true, explicitRoot: false}
        return new Promise((resolve, reject) => {
            return parseString(xml,options, function (err, result) {
                return !err ? resolve(result) : reject(err);
            });
        })
    };
}
