import fs from "fs";
import {l} from './logger';

const CONFIG_FILE = 'config/config.json'; 

l.info('test', CONFIG_FILE)
let config = JSON.parse(fs.readFileSync(CONFIG_FILE, 'utf8'));

export default config;

export class Config {
    public static getConfig() {
        return config[process.env.ENVIRONMENT || 'development'];
    }
    public static getKey(key) {
        return Config.getConfig()[key];
    }
}
