export interface HttpServiceIface {
    request(options): Promise<any>;
    get(url, options?): Promise<any>;
    post(url, options?): Promise<any>;
    put(url, options?): Promise<any>;
    delete(url, options?): Promise<any>;
    patch(url, options?): Promise<any>;
    parseOptions(url: any, method: string, options: any): any;
}

