import {Container, ContainerModule} from "inversify";
import "reflect-metadata";
import {TYPES} from "./types";
import {HttpServiceIface} from "./interfaces";
import HttpService from "../services/infrastructure/HttpService";

const container = new Container();

const modelDependencies = new ContainerModule((bind) => {
    bind<HttpServiceIface>(TYPES.HttpService).to(HttpService).inSingletonScope();
});


container.load(modelDependencies);

export { container };
