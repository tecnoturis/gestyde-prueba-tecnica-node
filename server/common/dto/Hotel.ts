export class Hotel{
    public hotelId: string;
    public name: string;
    public description: string;
    public location: Location;
    public city: string;
    public address: string;
    public province: string;
    public country: string;
    public postalCode: string;
    public phones: Phone[];
    public category: string;
    public photos: string[];
    public rooms: Room[];
    public currency: string;
}

export class Location{
    public latitude: string;
    public longitude: string;
}

export class Room {
    public roomId: string;
    public name: string;
    public rates: RoomRate[];
}

export class RoomRate{
    public rateId: string;
    public name: string;
    public price: number;
}

export class Phone {
    public number: string;
    public type: string;
}
