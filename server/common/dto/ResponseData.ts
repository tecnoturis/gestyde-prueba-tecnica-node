import Dtos from "./Dtos";

export default class ResponseData {
    public status: number;
    public data: Dtos;
}
