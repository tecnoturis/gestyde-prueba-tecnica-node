import {Hotel} from "./Hotel";

export default class Dtos {
    public rows: Hotel[];
    public count: number;
}
