import { Request, Response } from 'express';
import pino from 'pino';

// pino is not typescript, without this any, the code will not compile

const opts = {
    level: 'trace'
};

class Logger{
    private readonly l;

    public constructor(path: string = './logs'){
        this.l = pino(opts, (pino as any).destination(path));
        this.info('Logger up and running');
    }

    public trace(...args: any[]){
        this.l.trace(...args);
    }

    public debug(...args: any[]){
        this.l.debug(...args);
    }

    public info(...args: any[]){
        this.l.info(...args);
    }

    public warn(...args: any[]){
        this.l.warn(...args);
    }

    public error(...args: any[]){
        this.l.error(...args);
    }
}


class ServiceLogger {
    private readonly l: Logger;

    public constructor(l: Logger){
        this.l = l;
    }
    public requestLog(req: Request): void {
        this.l.info("Request: ", {
            url: req.url,
            headers: req.headers,
            params: req.params,
            query: req.query,
            body: req.body
        });
    }

    public responseLog(res: Response): void {
        this.l.info("Response: ", {statusCode: res.statusCode});
    }
};

const l = new Logger();
const s = new ServiceLogger(l);

export {Logger, ServiceLogger, l, s};
