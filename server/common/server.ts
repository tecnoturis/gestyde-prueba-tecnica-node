import 'reflect-metadata';
import express, {Application, Request, Response} from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import http from 'http';
import os from 'os';
import cookieParser from 'cookie-parser';
import swagger from './swagger/index';

import {l, s} from './logger';

const app = express();

export default class ExpressServer {
    public constructor() {
        const root = path.normalize(__dirname + '/../..');
        app.set('appPath', root + 'client');
        app.use(bodyParser.json({ limit: process.env.REQUEST_LIMIT || '100kb' }));
        app.use(bodyParser.urlencoded({ extended: true, limit: process.env.REQUEST_LIMIT || '100kb' }));
        app.use((req: Request, res: Response, next: Function) => {
            s.requestLog(req);
            res.on('close', () => s.responseLog(res));
            next();
        })
        app.use(cookieParser(process.env.SESSION_SECRET));
        app.use(express.static(`${root}/public`));
    }

    public router(routes: (app: Application) => void): ExpressServer {
        swagger(app, routes);
        return this;
    }

    public listen(p: string | number = process.env.PORT): Application {
        const welcome = port => () => l.info(`up and running in ${process.env.NODE_ENV || 'development'} @: ${os.hostname() } on port: ${port}}`);
        http.createServer(app).listen(p, welcome(p));
        return app;
    }
}
