import { Request, Response } from 'express';
import * as fs from "fs";

const availabilityMock  = "/mocks/hotel_avail_response.xml";
const hotelsMock  = "/mocks/hotel_list_response.xml";

export class Controller {
    public availability(req: Request, res: Response): void {
        res.set('Content-Type', 'text/xml');
        const response = fs.readFileSync(__dirname + availabilityMock);
        res.status(200).send(response);
    }

    public hotels(req: Request, res: Response): void {
        res.set('Content-Type', 'text/xml');
        const response = fs.readFileSync(__dirname + hotelsMock);
        res.status(200).send(response);
    }
}

export default new Controller();
