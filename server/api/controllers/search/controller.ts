import {Request, Response} from 'express';
import HotelService from "../../../common/services/application/HotelService";
import ResponseData from "../../../common/dto/ResponseData";

export class Controller {
    public list(req: Request, res: Response): void {
        let service = new HotelService();
        res.set('Content-Type', 'application/json');
        service.search(req.query).then((response: ResponseData) => {
            res.status(response.status);
            res.json(response.data);
        }).catch((err: ResponseData | Error) => {
            res.status(500).json(err);
        })
    }
}

export default new Controller();
