import {Request, Response} from 'express';

export class Controller {

    public health(req: Request, res: Response): void {
        res.set('Content-Type', 'application/json');
	    res.send({ok: true});
    }
}

export default new Controller();
