import {Application} from 'express';
import healthRouter from './api/controllers/health/router'
import searchRouter from './api/controllers/search/router'
import mockRouter from './api/controllers/mock/router'

export default function routes(app: Application): void {
    app.use('/api/v1', healthRouter);
    app.use('/api/v1/mock', mockRouter);
    app.use('/api/v1/hotels', searchRouter);
};
