import s from 'shelljs';
const config = require('./tsconfig.json');
const outDir = config.compilerOptions.outDir;
const configDir = outDir + "/config";

s.rm('-rf', outDir);
s.mkdir(outDir);
s.mkdir(configDir);
s.cp('.env', `${outDir}/.env`);
s.cp('config/config.json', `${configDir}/config.json`);
s.mkdir('-p', `${outDir}/api/controllers/mock/mocks`)
s.cp('server/api/controllers/mock/mocks/*xml', `${outDir}/api/controllers/mock/mocks`);
s.mkdir('-p', `${outDir}/common/swagger`);
