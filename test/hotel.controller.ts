import 'mocha';
import { expect } from 'chai';
import nock from "nock";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const inspect = require('eyes').inspector({styles: {all: 'magenta'}});
import request from 'supertest';
import Server from '../server';

const hotelAvailabilityResponse = '/responses/w2m/hotel_avail_response.xml';
const hotelListResponse = '/responses/w2m/hotel_list_response.xml';





describe('Hotels', () => {
    before(()=> {
        nock.disableNetConnect();
        nock.enableNetConnect('127.0.0.1');
    });

    afterEach(()=> {
        nock.cleanAll();
    });

    describe('Hotel list', () => {

        describe('Response OK', () => {
            const req: any = {};
            req.checkIn = "2019-08-20";
            req.checkOut = "2019-08-22";
            req.locality = "Madrid";
            req.locality = "Madrid";
            req.country = "España";
            req.adults = 2;
            req.children = [7];


            beforeEach((done)=>{
                nock('https://localhost:3000/api/v1/mock')
                    .post('/availability')
                    .replyWithFile(200, __dirname + hotelAvailabilityResponse, {
                        'Content-Type': 'text/xml',
                    })
                    .post('/hotels')
                    .replyWithFile(200, __dirname + hotelListResponse, {
                        'Content-Type': 'text/xml',
                    });

                done();
            });

            afterEach(()=> {
                nock.cleanAll();
            });

            it('should return 20 hotels',  async () =>{
                const r = await request(Server)
                    .get('/api/v1/hotels')
                    .query(req);

                expect(r.status).to.equal(200);
                expect(r.body).to.not.equal(undefined);

                let data = r.body;
                expect(data).to.not.equal(undefined);
                expect(data.rows).to.not.equal(undefined);
                expect(data.rows.length).to.equal(20);
                expect(data.rows.length).to.equal(data.count);

            });


            it('should show the first hotel', async () => {
                const r = await request(Server)
                    .get('/api/v1/hotels')
                    .query(req);

                let data = r.body;
                expect(data).to.not.equal(undefined);
                expect(data.rows).to.not.equal(undefined);
                const hotel = data.rows[0];
                expect(hotel.hotelId).to.equal('JP034689');
                expect(hotel.category).to.equal('2 Estrellas');
                expect(hotel.name).to.equal('Hostal Ballesta');
                expect(hotel.country).to.equal(' Spain');
                expect(hotel.phones.length).to.equal(0);
                expect(hotel.address).to.equal('Calle de la Ballesta, 5, 28004 Madrid, Spain');
                expect(hotel.postalCode).to.equal('28004');
                expect(hotel.currency).to.equal("EUR");
                expect(hotel.photos.length).to.equal(0);
                expect(hotel.rooms.length).to.equal(1);
                const room = hotel.rooms[0];
                expect(room.roomId).to.equal('DBT.ST');
                expect(room.name).to.equal('DOBLE 1 O 2 CAMAS STANDARD');
                expect(room.rates.length).to.equal(1);
                const rate = room.rates[0];
                expect(rate.rateId).to.equal('SAfFYfnb7x5CcPslSDQXq7A6ZMeaXsgqgTbL1RuquxSYOpE11YNVn6wawofu9wiKMmx+lTwQ0Nd39x4McmA4GOK4eup6Yx0SrgS3fzwqZ3fwxxTpF4I9IRoDhMrHGbQHULKG1drNY9HO6lqAzdRQPk2HiguCFAq+T5U1eOCkkJSZqxmeYS9LjO/dJaepanpju6Cu76CiUueyX8jUTPorjalCL0Gousudw9pMa04YitkiU2iSaF7DI49PX5Kwo7bUUwGlWfNdpKEFxH4nq55HiTlUOWQyzWUNqhY6Nz506WzQmr1lcka8df1ktzebOWLJbDqIrZuZ9YHDWszjdnRiQr+uIMPvQ1i3l0stBKfR9JUvDNhXyxak2tWUw12cToHtvP+llmYJ2meRw5cyPW2LnrkpUqex+hC2Q6LWLPuy66ln5TzlgnLgkBCftucd+p9tfv6/dO2k1eSvgd5o9i6fyXQ0jPXUyGn2QE17nswb861Zfv1bTVpVsPr48N6rsZoGJNNgaMpIR1DjV3xiQP7K2x3Vt8oU/pMoHgWWJ9EkXVxYAvDG9+lKzIn7hVRnBxkB');
                expect(rate.name).to.equal('SOLO HABITACIÓN');
                expect(rate.price).to.equal('77.18');
            });

            it('should show the last hotel', async () => {
                const r = await request(Server)
                    .get('/api/v1/hotels')
                    .query(req);

                let data = r.body;
                expect(data).to.not.equal(undefined);
                expect(data.rows).to.not.equal(undefined);
                const hotel = data.rows[19];
                expect(hotel.hotelId).to.equal('JP916505');
                expect(hotel.category).to.equal('4 Estrellas');
                expect(hotel.name).to.equal('Hotel City House Florida Norte');
                expect(hotel.country).to.equal(' Spain');
                expect(hotel.phones.length).to.equal(1);
                expect(hotel.phones[0].number).to.equal('#34915428300');
                expect(hotel.phones[0].type).to.equal('GEN');
                expect(hotel.address).to.equal('Paseo de la Florida, 5, 28008 Madrid, Spain');
                expect(hotel.postalCode).to.equal('28008');
                expect(hotel.currency).to.equal('EUR');
                expect(hotel.photos.length).to.equal(12);
                expect(hotel.photos[0]).to.equal('http://fotos.abreu.pt/fotos/www2/050/00005073.JPG');
                expect(hotel.photos[11]).to.equal('http://fotos.abreu.pt/fotos/www2/1105/00110550.JPG');
                expect(hotel.rooms.length).to.equal(1);
                const room = hotel.rooms[0];
                expect(room.roomId).to.equal('DBL.C3');
                expect(room.name).to.equal('DOBLE CAPACIDAD 3');
                expect(room.rates.length).to.equal(1);
                const rate = room.rates[0];
                expect(rate.rateId).to.equal('Us0+5WvWp9LXdsnjvyZNSFv4XzkISNZvbjIN9v2KpRin0kyNgAOqLSvnOYEnfkSYl3XyKbk1bPSXM2k/P/Y7KG+FEhNBoNiw4oUBqSFWPOTST3s3o4pp0WajlVQUmaHD8qW0TFp0pbR5A6Fzqpf6/NnE8hvEOXFjzCxeFGnF+rldVtb1a7WPhJsZcWedsvkO11JuwDHmqrv9X7xuYSjuQGLMHNPnwoux0A4rgq3O8sGQFUgSvtQATxaaqD1QoIG5nZ0LQiSuVhlNdWLDGR9lHGRW/kpD7hb1StQOk9XV+/8JFslIhMib3yiUn7y5/YIepfpRvSwCepbIlWEkgv0IhTjc2yJHAHELupHUvkPzZRRvRXi0d01ntuGV8mfwuhAXFudwTqhSSIAb3ncGrH8MeqFDAS2RzvgcsPWZx7TBBB9nyGpSLpdI/E7PB6IHEKm4RGVcQrIfwIlKVleUWL4JZ6qOtjjzD2VVERZCVyHRF+cZNLDVOL51x+PMxGujEfDKJ7jzEmz/VyWap1V4kBzJUZtrKZ6JpuSzRdMJsK//5dY/QdzxftGJtY/tZtZ9x8rq');
                expect(rate.name).to.equal('ALOJAMIENTO Y DESAYUNO');
                expect(rate.price).to.equal('126.08');
            });

        });

    });


});
