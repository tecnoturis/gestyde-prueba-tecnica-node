import 'mocha';
import { expect } from 'chai';
import request from 'supertest';
import Server from '../server';

describe('Health', () => {
    it('should return ok', () =>
        request(Server)
            .get('/api/v1/health')
            .expect('Content-Type', /json/)
            .then(r => {
                expect(r.body)
                    .to.be.an('object')
                    .that.has.property('ok')
                    .equal(true)
            }));

});
