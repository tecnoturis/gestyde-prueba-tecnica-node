import 'mocha';
import { container } from '../server/common/ioc/container';
import { TYPES } from '../server/common/ioc/types';
import nock from "nock";
import { HttpServiceIface } from '../server/common/ioc/interfaces';


describe('http service ', () => {
    describe('get', () => {
        it('should do get', (done) => {
            nock('http://localhost/')
                .get('/')
                .reply(200, {})

            container.get<HttpServiceIface>(TYPES.HttpService).get('http://localhost/').then(() => done())

        });
        it('should do post', (done) => {
            nock('http://localhost/')
                .post('/')
                .reply(200, {})

            container.get<HttpServiceIface>(TYPES.HttpService).post('http://localhost/').then(() => done())

        });
        it('should do put', (done) => {
            nock('http://localhost/')
                .put('/')
                .reply(200, {})

            container.get<HttpServiceIface>(TYPES.HttpService).put('http://localhost/').then(() => done())

        });
        it('should do get', (done) => {
            nock('http://localhost/')
                .delete('/')
                .reply(200, {})

            container.get<HttpServiceIface>(TYPES.HttpService).delete('http://localhost/').then(() => done())

        });
    })
});
